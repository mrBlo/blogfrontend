# Blog Web App

### Live URL
[https://selimvanlare.netlify.app](https://selimvanlare.netlify.app/)

## Table of contents
* [General Info](#markdown-header-general-info)
* [Description](#markdown-header-description)
* [App Screenshot](#markdown-header-app-screenshot)
* [Technologies](#markdown-header-technologies)


### General Info 
This is a blog application was developed using a **React JS**  for its frontend. This project is part of a full-stack app which uses a Headless CMS, Postgres and GraphQL.


### Description 
+ This project is pubished on netlify.
+ VERSION: v 1.0


### App Screenshot
![screenshot](./app_image.png)


### Technologies 
Project is created using:

* Apollo Client - for GraphQL calls and In-Memory Caching
* Bootstrap
* Moments - for dateTime formatting
* MarkDownIt - for markdown conversion


### Future Additions
* Implement push notifications


### Who do I talk to? 
* Repo owner : `isaac.afrifa3@yahoo.com`


